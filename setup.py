import centrum
from centrum.pkg import setup

from setuptools import find_packages

setup(
    name="centrum",
    version_py="centrum/version.py",
    description="Utilities for rapid development of data-centric web and console applications.",
    author="Cory Giles",
    author_email="mail@corygil.es",
    packages=find_packages(),
    entry_points={
        "console_scripts": [
            "centrum=centrum.cli:cli",
            "mmat=centrum.mmat:cli",
            "dmat=centrum.dmat:cli"
        ]
    }
)
