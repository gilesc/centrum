#!/usr/bin/env python

import os
import itertools
import pickle
import sqlite3
import sys
import multiprocessing as mp

import click
import numpy as np 
import pandas as pd

class Metadata(object):
    def __init__(self, path):
        create = not os.path.exists(path)
        self.db = sqlite3.connect(path)
        if create:
            c = self.db.cursor()
            c.execute("""CREATE TABLE metadata (
                k VARCHAR,
                v BLOB
            );""")
            self.db.commit()

    def __del__(self):
        self.close()

    def close(self):
        self.db.close()

    def __setitem__(self, k, v):
        c = self.db.cursor()
        c.execute("""INSERT INTO metadata(k, v) VALUES(?, ?)""",
                (k, pickle.dumps(v)))
        self.db.commit()

    def __getitem__(self, k):
        c = self.db.cursor()
        c.execute("""SELECT v FROM metadata WHERE k=?""", (k,))
        try:
            return pickle.loads(next(c)[0])
        except:
            raise KeyError

    def __contains__(self, k):
        try:
            self.__getitem__(k)
            return True
        except KeyError:
            return False


class MatrixStore(object):
    def __init__(self, path):
        self.root = path
        os.makedirs(path, exist_ok=True)

    def __getitem__(self, name):
        if str(name) in os.listdir(self.root):
            return Matrix(self, name)
        raise KeyError

    def __iter__(self):
        for name in os.listdir(self.root):
            yield Matrix(self, name)

    def new(self, name, columns):
        return Matrix(self, name, columns=columns)

def key_directory(matrix_root, key):
    if len(key) >= 4:
        return os.path.join(matrix_root, "data", key[:2], key[2:4])
    else:
        return os.path.join(matrix_root, "data", "_")

def matrix_add(args):
    matrix_root, line = args
    fields = line.split("\t")
    key = fields[0]
    data = np.array(list(map(float, fields[1:])), dtype=np.float64)
    dir = key_directory(matrix_root, key)
    os.makedirs(dir, exist_ok=True)
    path = os.path.join(dir, key)
    h = np.memmap(path, dtype=np.float64, mode="w+", shape=data.shape)
    h[:] = data

class Matrix(object):
    def __init__(self, store, name, columns=None):
        self.store = store
        self.name = str(name)

        os.makedirs(os.path.join(self.path, "data"), exist_ok=True)
        self.meta = Metadata(os.path.join(self.path, "metadata.db"))
        if columns is not None:
            self.meta["columns"] = columns
        self.columns = self.meta["columns"]
        if "index" not in self.meta:
            index = []
            for dir, dirs, files in os.walk(os.path.join(self.path, "data")):
                index.extend(files)
            self.meta["index"] = pd.Index(index)
        self.index = self.meta["index"]

        self._maps = {}

    @property
    def path(self):
        return os.path.join(self.store.root, self.name)

    @property
    def shape(self):
        return (len(self.index), len(self.columns))
 
    def __del__(self):
        self.meta.close()
   
    def __getitem__(self, key):
        key = str(key)
        if key not in self._maps:
            path = os.path.join(key_directory(self.path, key), key)
            self._maps[key] = np.memmap(path, dtype=np.float64, mode="r")
        return self._maps[key]

    def __contains__(self, key):
        key = str(key)
        return key in self.index

@click.group()
def cli():
    pass

@cli.command("import")
@click.argument("db_path")
@click.option("--taxon-id", "-t", required=True, type=int)
def _import(db_path, taxon_id):
    chunk_size = 100
    store = MatrixStore(db_path)

    lines = iter(sys.stdin)
    columns = next(lines).split("\t")[1:]

    try:
        X = store[taxon_id]
    except KeyError:
        X = store.new(taxon_id, columns)

    mp.set_start_method("forkserver")
    p = mp.Pool()
    total = 0
    for _ in p.imap_unordered(matrix_add,
            ((X.path, line) for line in lines),
            chunksize=10):
        total += 1
        if total % 10 == 0:
            print(total)

@cli.command()
@click.argument("db_path")
def info(db_path):
    store = MatrixStore(db_path)
    print("Matrix", "Rows", "Columns", sep="\t")
    for matrix in store:
        print(matrix.name, *matrix.shape, sep="\t")

if __name__ == "__main__":
    cli()
