__all__ = ["ResourceLookup", "path", "handle"]

from os.path import abspath, dirname, join

class ResourceLookup(object):

    def __init__(self, pkgname):
        module = __import__(pkgname)
        self._basedir = abspath(join(dirname(module.__file__), 
            "..", "resources"))

    def path(self, relpath):
        """
        Get an absolute path to a data resource.
        """
        return join(self._basedir, relpath)

    def handle(self, relpath, mode="r"):
        """
        Get a file handle to a data resource.
        """
        return open(self.path(relpath), mode=mode)

_lookup = ResourceLookup("centrum")
path = _lookup.path
handle = _lookup.handle
