import os
import subprocess as sp
import sys

from centrum.io import ChangeDirectory, set_executable

import setuptools

def git_version(dir=".", write_to=None, commit_as_patch_number=False):
    """
    Get a package's version from git tags.
    """
    cmd = ["git", "describe", "--tags"]
    if commit_as_patch_number:
        cmd.append("--long")

    with ChangeDirectory(dir):
        o = sp.check_output(cmd).decode(sys.getdefaultencoding()).strip()

    if commit_as_patch_number:
        o = ".".join(o.split("-")[:2])

    if write_to is not None:
        with open(write_to, "w") as h:
            h.write("__version__ = '%s'" % o)

    return o

TEST_HOOK = """
#!/usr/bin/env bash

python setup.py test
""".lstrip()

VERSION_HOOK = """
#!/usr/bin/env python

from centrum.pkg import git_version

git_version(commit_as_patch_number=%s, write_to='%s')
""".lstrip()

def install_git_hooks(package_root=".", version_py=None, 
        test_hook=True, commit_as_patch_number=False):
    """
    Install hooks in a git repository to automatically update
    a version file before commits and after checkout, and run tests.
    """
    with ChangeDirectory(package_root):
        hooks_dir = os.path.join(".git", "hooks")
        if not os.path.exists(hooks_dir):
            raise IOError("%s is not a git repository")

        def write_hook(hook, contents):
            hook_path = os.path.join(hooks_dir, hook)
            with open(hook_path, "w") as h:
                h.write(contents)
            set_executable(hook_path)
        
        installed_hooks = []

        if test_hook:
            installed_hooks.append("test")
            write_hook("pre-commit", TEST_HOOK)

        if version_py is not None:
            installed_hooks.append("version")
            version_hook = VERSION_HOOK % \
                    (commit_as_patch_number, version_py)
            write_hook("post-commit", version_hook)
            write_hook("post-update", version_hook)

        print("Hooks installed:", ", ".join(installed_hooks), file=sys.stderr)

def setup(*args, version_py=None, **kwargs):
    """
    Extension of setuptools.setup with more features:
    
    -  autodetection of __version__ and autowriting of version.py from
        git repository
    """
    if version_py is not None:
        kwargs["version"] = git_version(write_to=version_py, 
            commit_as_patch_number=kwargs.get("commit_as_patch_number", True))
    setuptools.setup(*args, **kwargs)
