__all__ = ["FileLock", "ChangeDirectory"]

import os
import stat

import portalocker

class FileLock(object):
    """
    A context manager for locking a file.
    """
    FLAGS = {
            "shared": portalocker.LOCK_SH,
            "exclusive": portalocker.LOCK_EX,
            "nonblocking": portalocker.LOCK_NB
    }

    def __init__(self, path, type="exclusive"):
        self.type = self.FLAGS[type] 
        self.path = path

    def __enter__(self):
        self.handle = open(self.path, "r")
        portalocker.lock(self.handle, self.type)

    def __exit__(self, type, value, traceback):
        portalocker.unlock(self.handle)
        self.handle.close()

class ChangeDirectory(object):
    """
    A context manager for temporarily changing working directory.
    """
    def __init__(self, path):
        self.path = os.path.abspath(path)

    def __enter__(self):
        self.original_path = os.path.abspath(os.curdir)
        os.chdir(self.path)
    
    def __exit__(self, type, value, traceback):
        os.chdir(self.original_path)

def set_executable(path):
    """
    Set the user executable bit on a file.
    """
    st = os.stat(path)
    os.chmod(path, st.st_mode | stat.S_IEXEC)
