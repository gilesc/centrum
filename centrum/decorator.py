"""
Function decorators.
"""

__all__ = ["not_implemented"]

from functools import wraps

def not_implemented(fn):
    """
    Annotate a function as being not yet usable.
    """
    @wraps(fn)
    def wrapped(*args, **kwargs):
        msg = "Function %s.%s is not yet implemented." \
                % (fn.__module__, fn.__name__)
        raise NotImplementedError(msg)
    return wrapped
