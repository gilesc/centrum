from click import group, option

@group()
def cli():
    """
    Python development utilities.
    """
    pass

#############
# centrum pkg
#############

from centrum import pkg

@cli.group("pkg")
def cli_pkg():
    """
    Utilities for maintaining Python packages.
    """
    pass

@cli_pkg.command("install-git-hooks")
@option("--version-py", "-v", type=str, 
        help="Path (relative to package root) to the version.py \
file that the __version__ should be written to.")
@option("--commit-as-patch-number", "-c", 
        is_flag=True,
        help="Use the number of commits since the latest tag as \
the semver patch number (0.0.x)")
@option("--test-hook/--no-test-hook", default=True,
        help="Add hook to run 'python setup.py test' before each commit.")
@option("--package-root", "-r", default=".", 
    help="Path to the package/repository root (default '.')")
def install_git_hooks(**kwargs):
    """
    Add hooks to update version_py, run tests, etc.
    """
    pkg.install_git_hooks(**kwargs)
