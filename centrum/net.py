__all__ = ["fetch_url", "open_url"]

import io
import os
import base64
import urllib.request

CACHE_DIR = os.path.expanduser("~/.cache/centrum")
os.makedirs(CACHE_DIR, exist_ok=True)

def fetch_url(url):
    code = base64.b64encode(str(hash(url)).encode("utf-8")).decode("utf-8")
    path = os.path.join(CACHE_DIR, code)
    if not os.path.exists(path):
        with urllib.request.urlopen(url) as input, open(path, "wb") as output:
            while True:
                data = input.read(4096)
                if not data:
                    break
                output.write(data)
    return path

def open_url(url, mode="r"):
    assert("r" in mode)
    path = fetch_url(url)
    return open(path, mode=mode)
