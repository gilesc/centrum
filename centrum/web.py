__all__ = ["Application", "link"]

import bottle

from mako.lookup import TemplateLookup

from centrum import resource

class Application(bottle.Bottle):
    def __init__(self, template_dirs=[]):
        dirs = [resource.path("template")]
        dirs.extend(template_dirs)
        self._lookup = TemplateLookup(directories=template_dirs)
        super(Application, self).__init__()

    def view(self, template_path):
        tmpl = self._lookup.get_template(template_path)
        def wrapper(**kwargs):
            return tmpl.render(**kwargs)
        return wrapper

def link(text, url):
    return '<a href="%s">%s</a>' % (url, text)
